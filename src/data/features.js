import ZaciskarkaA from "./graphics/ZaciskarkaA.png";
import ZaciskarkaB from "./graphics/ZaciskarkaB.png";
import ZaciskarkaC from "./graphics/ZaciskarkaC.png";

import ZłączeA from "./graphics/ZłączeA.png";
import ZłączeB from "./graphics/ZłączeB.png";
import ZłączeC from "./graphics/ZłączeC.png";

import PrzewódA from "./graphics/PrzewódA.png";
import PrzewódB from "./graphics/PrzewódB.png";
import PrzewódC from "./graphics/PrzewódC.png";

const typesData = [
  {
    key: "A",
    name: ZaciskarkaA,
    level: ["easy"],
  },
  {
    key: "B",
    name: ZaciskarkaB,
    photo: ZaciskarkaB,
    level: ["easy"],
  },
  {
    key: "C",
    name: ZaciskarkaC,
    photo: ZaciskarkaC,
    level: ["easy"],
  },
];

const connectorsData = [
  {
    key: "A",
    name: ZłączeA,
    photo: ZłączeA,
    level: ["easy"],
  },
  {
    key: "B",
    name: ZłączeB,
    photo: ZłączeB,
    level: ["easy"],
  },
  {
    key: "C",
    name: ZłączeC,
    photo: ZłączeC,
    level: ["easy"],
  },
];

const wiresData = [
  {
    key: "A",
    name: PrzewódA,
    photo: PrzewódA,
    level: ["easy"],
  },
  {
    key: "B",
    name:PrzewódB,
    photo: PrzewódB,
    level: ["easy"],
  },
  {
    key: "C",
    name: PrzewódC,
    photo: PrzewódC,
    level: ["easy"],
  },
];

// const typesData = [
//   {
//     key: "A",
//     name: (
//       <div style={{ display: "flex" }}>
//         <p style={{ margin: "50px 10px 0 0" }}>A</p>
//         <img style={{ width: "100%", height: "120px" }} src={ZaciskarkaA} />
//       </div>
//     ),
//     photo: ZaciskarkaA,
//     level: ["easy"],
//   },
//   {
//     key: "B",
//     name: (
//       <div style={{ display: "flex" }}>
//         <p style={{ margin: "50px 10px 0 0" }}>B</p>
//         <img style={{ width: "100%", height: "120px" }} src={ZaciskarkaB} />
//       </div>
//     ),
//     photo: ZaciskarkaB,
//     level: ["easy"],
//   },
//   {
//     key: "C",
//     name: (
//       <div style={{ display: "flex" }}>
//         <p style={{ margin: "50px 10px 0 0" }}>C</p>
//         <img style={{ width: "100%", height: "120px" }} src={ZaciskarkaC} />
//       </div>
//     ),
//     photo: ZaciskarkaC,
//     level: ["easy"],
//   },
// ];

// const connectorsData = [
//   {
//     key: "A",
//     name: (
//       <div style={{ display: "flex" }}>
//         <p style={{ margin: "50px 10px 0 0" }}>A</p>
//         <img style={{ width: "100%", height: "120px" }} src={ZłączeA} />
//       </div>
//     ),
//     photo: ZłączeA,
//     level: ["easy"],
//   },
//   {
//     key: "B",
//     name: (
//       <div style={{ display: "flex" }}>
//         <p style={{ margin: "50px 10px 0 0" }}>B</p>
//         <img style={{ width: "100%", height: "120px" }} src={ZłączeB} />
//       </div>
//     ),
//     photo: ZłączeB,
//     level: ["easy"],
//   },
//   {
//     key: "C",
//     name: (
//       <div style={{ display: "flex" }}>
//         <p style={{ margin: "50px 10px 0 0" }}>C</p>
//         <img style={{ width: "100%", height: "120px" }} src={ZłączeC} />
//       </div>
//     ),
//     photo: ZłączeC,
//     level: ["easy"],
//   },
// ];

// const wiresData = [
//   {
//     key: "A",
//     name: (
//       <div style={{ display: "flex" }}>
//         <p style={{ margin: "50px 10px 0 0" }}>A</p>
//         <img
//           style={{ width: "80%", height: "120px", padding: "25px 0" }}
//           src={PrzewódA}
//         />
//       </div>
//     ),
//     photo: PrzewódA,
//     level: ["easy"],
//   },
//   {
//     key: "B",
//     name: (
//       <div style={{ display: "flex" }}>
//         <p style={{ margin: "50px 10px 0 0" }}>B</p>
//         <img
//           style={{ width: "80%", height: "120px", padding: "25px 0" }}
//           src={PrzewódB}
//         />
//       </div>
//     ),
//     photo: PrzewódB,
//     level: ["easy"],
//   },
//   {
//     key: "C",
//     name: (
//       <div style={{ display: "flex" }}>
//         <p style={{ margin: "50px 10px 0 0" }}>C</p>
//         <img
//           style={{ width: "80%", height: "120px", padding: "25px 0" }}
//           src={PrzewódC}
//         />
//       </div>
//     ),
//     photo: PrzewódC,
//     level: ["easy"],
//   },
// ];

export { typesData, connectorsData, wiresData };
