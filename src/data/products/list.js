import ŚciąganieIzolacjiA from "../graphics/ŚciąganieIzolacjiA.png";
import ŚciąganieIzolacjiB from "../graphics/ŚciąganieIzolacjiB.png";
import ŚciąganieIzolacjiC from "../graphics/ŚciąganieIzolacjiC.png";

const InsulationData = [
  {
    key: "A",
    name: ŚciąganieIzolacjiA,
    photo: ŚciąganieIzolacjiA,
    level: ["easy"],
  },

  {
    key: "B",
    name: ŚciąganieIzolacjiB,
    photo: ŚciąganieIzolacjiB,
    level: ["easy"],
  },
  {
    key: "C",
    name: ŚciąganieIzolacjiC,
    photo: ŚciąganieIzolacjiC,
    level: ["easy"],
  },
];

export default InsulationData;
