const initialState = {
  level: "easy",

  easy: {
    insulationStripping: null,
    clamping: null,
    connectors: null,
    wire: null,
    photo: null,
  },
};

export default initialState;
