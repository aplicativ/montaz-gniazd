import React, { useContext, useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import { Typography } from "@material-ui/core";

import DataContext from "@context/DataContext";
import insulationData from "@data/products/list";
import dataHelper from "@lib/data";

const useStyles = makeStyles({
  image: {
    width: "100%",
    maxWidth: "300px",
  },
});

const Product = () => {
  const classes = useStyles();
  const { data } = useContext(DataContext);
  const product = dataHelper.findItem(insulationData, data.product);
  return <div></div>;
};

export default Product;
