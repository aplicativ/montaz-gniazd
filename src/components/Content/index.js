import React, { useContext } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import { Typography } from "@material-ui/core";

import Product from "./subcomponents/product";
import Type from "./subcomponents/type";
import Fabric from "./subcomponents/fabric";
import Validation from "@components/Validation";

import DataContext from "@context/DataContext";
import Table from "../Table";
const useStyles = makeStyles({
  content: {
    display: "flex",
    alignItems: "end",
    flexDirection: "column",
    padding: "10px",
    marginBottom: "30px",
    width: "97%",
  },
});

const Content = () => {
  const classes = useStyles();
  const { data } = useContext(DataContext);
  const { product } = data;
  return (
    <>
            <h4 style={{padding:"20px",textAlign:"center"}}>
          Twoje ćwiczenie polega na skompletowaniu podstawowych narzędzi i
          materiałów do wykonania przewodu koncentrycznego zakończonego wtykami
          kompresyjnymi typu F. Przewód z żyłą wewnętrzną z drutu miedzianego,
          ekran wykonant z folii AL-PET z oplotem 90%. Wybierz z każdej bazy
          jeden element. Oznaczającą literę wpisz do tabeli zatytuowanej
          "Zależności pomiędzy parametrami".
        </h4>
      <Table />
      {/* {product && ( */}

      <Paper elevation={3} className={classes.content}>
        <Validation />
      </Paper>
      {/* )} */}
    </>
  );
};

export default Content;
