import React, { useContext } from "react";
import { makeStyles } from "@material-ui/core/styles";
import DataContex from "../../context/DataContext";
import { Button } from "@material-ui/core";

import message from "@lib/message";
import dataHelper from "@lib/data";

import insulationData from "@data/products/list";
import initialState from "@data/initialState";

const useStyles = makeStyles((theme) => ({
  wrap: {
    marginTop: "20px",
  },

  button: {
    backgroundColor: "#90B84A",
    padding: "15px 20px",
    float: "right",
    "&:hover": {
      backgroundColor: "rgba(144, 184, 74, 0.9);",
    },
  },
}));

const Validation = () => {
  const classes = useStyles();
  const { data } = useContext(DataContex);
  const { clamping, connectors, insulationStripping, wire, photo } = data;

  const isValid = () => {
    let isValid = false;
    const selected = [clamping, connectors, insulationStripping, wire];

    if (
      clamping === null ||
      connectors === null ||
      insulationStripping === null ||
      wire === null
    ) {
      console.log(selected);
      message.warning();
    } else if (
      clamping !== "A" ||
      connectors !== "C" ||
      insulationStripping !== "C" ||
      wire !== "B"
    ) {
      console.log(clamping, connectors, insulationStripping, wire);
      message.error();
    } else {
      isValid = true;
    }
    return isValid;
  };

  const onClick = () => {
    console.log("click");
    console.log(data);
    if (isValid()) {
      message.success();

      //#todo zapytać
      // resetData();
    }
  };

  return (
    <div className={classes.wrap}>
      <Button className={classes.button} onClick={onClick} variant="contained">
        Sprawdź
      </Button>
    </div>
  );
};

export default Validation;
