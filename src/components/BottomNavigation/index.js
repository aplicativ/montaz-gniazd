import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import BottomNavigation from "@material-ui/core/BottomNavigation";
import BottomNavigationAction from "@material-ui/core/BottomNavigationAction";
import TrendingUpIcon from "@material-ui/icons/TrendingUp";
import EqualizerIcon from "@material-ui/icons/Equalizer";
import PageviewIcon from "@material-ui/icons/Pageview";
import GetAppIcon from "@material-ui/icons/GetApp";
import SaveIcon from "@material-ui/icons/Save";
import CameraAltIcon from "@material-ui/icons/CameraAlt";
import VolumeUpIcon from "@material-ui/icons/VolumeUp";
import Specification from "../Menu/subcomponents/specification";
const useStyles = makeStyles({
  root: {
    width: "100%",
    height: "65px",
    "& button": {
      paddingTop: "30px 0",
    },
  },
});

export default function SimpleBottomNavigation() {
  const classes = useStyles();
  const [value, setValue] = React.useState(0);

  return (
    <BottomNavigation
      value={value}
      onChange={(event, newValue) => {
        setValue(newValue);
      }}
      showLabels
      className={classes.root}
    >
      {/* <BottomNavigationAction
        label="Plansza interaktywna"
        icon={<TrendingUpIcon />}
      />
      <BottomNavigationAction
        label="Dokumentacja interaktywna"
        icon={<PageviewIcon />}
      /> */}
      <Specification />
      <BottomNavigationAction label="Pobierz" icon={<GetAppIcon />} />
      <BottomNavigationAction label="Zapisz" icon={<SaveIcon />} />
      <BottomNavigationAction label="Zrzut ekranu" icon={<CameraAltIcon />} />
      <BottomNavigationAction label="Odsłuchaj" icon={<VolumeUpIcon />} />
    </BottomNavigation>
  );
}
