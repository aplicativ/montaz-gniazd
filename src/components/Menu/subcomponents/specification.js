import React from "react";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import { makeStyles } from "@material-ui/core/styles";
import { BrowserRouter as Router } from "react-router-dom";
import BottomNavigationAction from "@material-ui/core/BottomNavigationAction";
import TrendingUpIcon from "@material-ui/icons/TrendingUp";
import CreateIcon from "@material-ui/icons/Create";
const useStyles = makeStyles((theme) => ({
  dialog: {
    "& div": {
      "& div": {
        maxWidth: "100%",
      },
    },
  },
  submitcancel: {
    textAlign: "right",
    "& button": {
      color: "#90B84A",
      border: "1px solid #90B84A",
      margin: "10px 15px",
      fontWeight: "600",
      padding: "5px 25px",
      "& a": {
        color: "#b54d2d",
        textDecoration: "none",
      },
      "&:hover": {
        border: "1px solid #90B84A",
        backgroundColor: "rgba(144, 184, 74, 0.1);",
      },
    },
  },
  addResourceMaterial: {
    padding: "10px",
    borderRadius: "5px",
    border: "none",
    margin: "15px 0",
    backgroundColor: "rgba(109, 47, 27, 0.3)",
    "& a": {
      textDecoration: "none",
      color: "#000",
    },
  },

  task: {
    padding: "10px",
  },

  table: {
    textAlign: "center",
  },

  wirePhoto: {
    maxWidth: "300px",
  },

  specification: {
    padding: "15px",
  },

  title: {
    fontWeight: "700",
    padding: "20px",
  },
}));

export default function Specification(props) {
  let { form } = props;
  const classes = useStyles();

  const [open, setOpen] = React.useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <Router>
      <BottomNavigationAction
        label="Specyfikacja przewodów"
        icon={<CreateIcon />}
        onClick={handleClickOpen}
        className={classes.patterns}
        showLabel
      />
      <Dialog
        className={classes.dialog}
        open={open}
        onClose={handleClose}
        aria-labelledby="form-dialog-title"
        style={{ zIndex: "999999" }}

      >
        <DialogContent>
          <div>
            <table border="1" className={classes.table}>
              <tbody>
                <tr>
                  <td className={classes.title}>Nazwa</td>
                  <td className={classes.title}>Wygląd</td>
                  <td className={classes.title}>Specyfikacja</td>
                </tr>
                <tr>
                  <td>Przewód koncentryczny typu RG6, CTF 113 DIGITAL</td>
                  <td>
                    <img src="https://www.atel.com.pl/pic_cache/08/07097.webp?1608674444" />
                  </td>
                  <td className={classes.specification}>
                    żyła wewnętrzna: 1,13 mm/drut miedziany (Cu) ; dielektryk:
                    4,8 mm/Spieniony fizycznie PE ; ekran: Folia Al-PET klejona
                    do dielektryka ; oplot: 90% ; powłoka zewnętrzna: 7,00
                    mm/PVC/biała; Tłumienność: 50 MHz - 3,9 dB/100 m, 100 MHz -
                    5,5 dB/100 m, 200 MHz - 7,8 dB/100 m, 450 MHz - 12,3 dB/100
                    m, 800 MHz - 16,6 dB/100 m, 1000 MHz - 18,5 dB/100 m, 1200
                    MHZ - 21,3 dB/100m, 1750 MHZ - 25,2 dB/100m, 2050 MHz - 27,4
                    dB/100 m, 2150 MHZ - 28,3 dB/100m, 2400 MHz - 29,8 dB/100 m
                  </td>
                </tr>
                <tr>
                  <td>Przewód koncentryczny RG-11/U Tri-shield, żel</td>
                  <td>
                    <img src="https://www.atel.com.pl/pic_cache/08/07099.webp?1608674442" />
                  </td>
                  <td className={classes.specification}>
                    żyła wewnętrzna: 1,63 mm/drut miedziany (Cu) ; dielektryk:
                    7,11 mm/Spieniony fizycznie PE; ekran: Folia Al-Pet + oplot
                    60% + klejona folia Al-Pet; powłoka zewnętrzna: 10,1
                    mm/PE/czarna + zabezpieczenie żelem; tłumienność: 50 MHz -
                    3,1 dB/100m, 100 MHz - 4,3 dB/100m, 200 MHz - 6,1 dB/100m,
                    450 MHz - 9,1 dB/100m, 860 MHz - 12,9 dB/100m, 1000 MHz -
                    14,3 dB/100m, 1350 MHz - 17,1 dB/100m, 1750 MHz - 20,1
                    dB/100m, 2150 MHz - 23,0 dB/100m, 2400MHz - 24,5 dB/100m,
                    3000 MHz - 37,3 dB/100m
                  </td>
                </tr>
                <tr>
                  <td>Przewód koncentryczny RG213, linka, czarny</td>
                  <td>
                    <img
                      src="https://www.atel.com.pl/pic_cache/08/08999.webp?1620645207"
                      className={classes.wirePhoto}
                    />
                  </td>
                  <td className={classes.specification}>
                    żyła wewnętrzna: 7x0,75 Cu/linka; dielektryk: 7,2mm, pianka
                    PE ; powłoka zewnętrzna: 10,3mm/PVC/czarna; impedancja: 50Ω;
                    pojemność jednostkowa: 98 ± 2 pF/m; tłumienność: 10 MHz -
                    1,7 dB/100m, 50 MHz - 4,6 dB/100m, 100 MHz - 6,7 dB/100m,
                    230 MHz - 9,8 dB/100m, 470 MHz - 15,3 dB/100m, 860 MHz -
                    22,3 dB/100m, 1000 MHz - 24,5 dB/100m, 1500 MHz - 31,3
                    dB/100m, 2000 MHz - 36,5 dB/100m
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        </DialogContent>
        <DialogActions className={`${classes.task} ${classes.submitcancel}`}>
          <Button onClick={handleClose} color="primary">
            Ok
          </Button>
        </DialogActions>
      </Dialog>
    </Router>
  );
}
