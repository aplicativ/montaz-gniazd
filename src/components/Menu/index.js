import React from "react";
import { makeStyles } from "@material-ui/core/styles";

import insulationData from "@data/products/list";
import { typesData, connectorsData, wiresData } from "@data/features";

import RadioGroupPrimary from "./subcomponents/RadioGroupPrimary";

const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
  },
  heading: {
    fontSize: theme.typography.pxToRem(15),
    fontWeight: theme.typography.fontWeightRegular,
  },

  formControlLabel: {
    "& span": {
      fontSize: "0.8em",
    },
  },
}));

const Menu = () => {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <RadioGroupPrimary
        items={insulationData}
        keyData="insulationStripping"
        title="Wybierz narzędzie do ściągania izolacji"
        key="insulationStripping"
      />
      <RadioGroupPrimary
        items={typesData}
        keyData="clamping"
        title="Wybierz narzędzie do zaciskania złączy"
        key="clamping"
      />
      <RadioGroupPrimary
        items={connectorsData}
        keyData="connectors"
        title="Wybierz złącza"
        key="connectors"
      />
      <RadioGroupPrimary
        items={wiresData}
        keyData="wire"
        title="Wybierz przewód"
        key="wire"
      />
    </div>
  );
};

export default Menu;
