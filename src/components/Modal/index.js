import React, { useContext, setState } from "react";
import { Button } from "@material-ui/core";
import clsx from "clsx";
import { makeStyles } from "@material-ui/core/styles";

import ModalContext from "@context/ModalContext";

import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';

import successIcon from "./icons/success.jpg";
import errorIcon from "./icons/error.png";
import warningIcon from "./icons/info.jpg";


const useStyles = makeStyles((theme) => ({
    backDropSuccess: {
        background: 'rgba(65,172,38,0.3)'
    },
    backDrop: {

    },
    icon: {
        textAlign: "center",
        marginBottom: "10px",
        width:"100%"
    },
    iconImg: {
        margin:"0 auto"
    },
    title: {
        textAlign: "center",
        position: "relative",
        maxWidth: "100%",
        marginBottom: "20px",
        color: "#595959",
        fontSize: "25px",
        fontWeight: "600",
        textAlign: "center",
        textTransform: "none",
        wordWrap: "break-word"
    },
    description: {
        color: "#595959",
        fontWeight: "600",
        textAlign: "center",
        fontSize: "16px",
        marginBottom: "20px",
    },
    buttonsWrap: {
        display: "flex",
        justifyContent: "center"
    },
    button: {
        margin: "10px"
    }
}));

const Modal = () => {
    let { modalParams, setModalParams } = useContext(ModalContext);
    const { type, title, text, isOpen } = modalParams;
    const classes = useStyles();

    const handleClose = () => {
        setModalParams({ ...modalParams, isOpen: false, type: null, title: null, text: null });
    }

    console.log('text', text);


    return (
        <Dialog
            fullWidth open={isOpen}
            onClose={handleClose}
            style={{ zIndex: "999999" }}
            BackdropProps={{
                classes: {
                    root: type == "success" ? classes.backDropSuccess : classes.backDrop
                }
            }}
        >
            <DialogContent style={{ padding: "25px" }}>

                {type === "success" && (
                    <div className={classes.icon}>
                        <img className={classes.iconImg} src={successIcon} />
                    </div>
                )}
                {type === "error" && (
                    <div className={classes.icon}>
                        <img className={classes.iconImg} src={errorIcon} />
                    </div>
                )}
                {type === "warning" && (
                    <div className={classes.icon}>
                        <img className={classes.iconImg} src={warningIcon} />
                    </div>
                )}


                {title && (
                    <div className={classes.title}>{title}</div>
                )}
                {text && (
                    <p className={classes.description}>{text}</p>
                )}

                <div className={classes.buttonsWrap}>
                    {type === "error" && (
                        <Button onClick={handleClose} variant="contained" className={classes.button} style={{ backgroundColor: '#1979c2', color: '#FFFFFF' }}>
                            Spróbuj jeszcze raz
                        </Button>
                    )}

                    {type === "success" && (
                        <>
                            <Button onClick={handleClose} variant="contained" className={classes.button} style={{ backgroundColor: '#357a38', color: '#FFFFFF' }}>
                                Wróć
                    </Button>
                            <Button onClick={handleClose} variant="contained" className={classes.button} color="primary">
                                Zapisz listę kroków
                    </Button>
                        </>
                    )}

                    {type === "warning" && (
                        <Button onClick={handleClose} variant="contained" className={classes.button} style={{ backgroundColor: '#1979c2', color: '#FFFFFF' }}>
                            Powrót
                        </Button>
                    )}
                </div>

            </DialogContent>
        </Dialog>
    )
};

export default Modal;

