import React, { useContext } from "react";
import clsx from "clsx";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import IconButton from "@material-ui/core/IconButton";
import MenuIcon from "@material-ui/icons/Menu";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";

import DataContex from "@context/DataContext";
import useStyles from "./styles";
import initialState from "@data/initialState";

const tabProps = (index) => {
  return {
    id: `simple-tab-${index}`,
    "aria-controls": `simple-tabpanel-${index}`,
  };
};

const Header = ({ isOpenNavigation, setIsOpenNavigation }) => {
  const classes = useStyles();
  const { setState, level, state } = useContext(DataContex);
  const handleLevelChange = (event, newValue) => {
    setState({ ...state, level: newValue });
  };

  return (
    <AppBar
      position="fixed"
      className={clsx(classes.appBar, {
        [classes.appBarShift]: isOpenNavigation,
      })}
    >
      <Toolbar className={classes.toolbar}>
        <IconButton
          color="inherit"
          aria-label="open drawer"
          onClick={() => setIsOpenNavigation(!isOpenNavigation)}
          edge="start"
          className={clsx(classes.menuButton, isOpenNavigation && classes.hide)}
        >
          <MenuIcon />
        </IconButton>
  
      </Toolbar>
    </AppBar>
  );
};

export default Header;
