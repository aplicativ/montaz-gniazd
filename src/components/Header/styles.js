import { makeStyles } from "@material-ui/core/styles";

const drawerWidth = 240;

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    justifyContent: "center",
  },
  appBar: {
    transition: theme.transitions.create(["margin", "width"], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    // position: "fixed",
    // left: "0",
    // top: "0",
    // display: "flex",
    // alignItems: "center",
    
    justifyContent: "center",
    backgroundColor: "#464343",
  },
  appBarShift: {
    // width: `calc(100% - ${drawerWidth}px)`,
    width:"100%",
    marginLeft: drawerWidth,
    transition: theme.transitions.create(["margin", "width"], {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  menuItem: {
    fontWeight: 600,
  },
  hide: {
    display: "none",
  },

  toolbar: {
    backgroundColor: "#334957",
  },
}));

export default useStyles;
