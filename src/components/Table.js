import React, { useContext } from "react";
import { DataGrid, plPL } from "@mui/x-data-grid";
import { makeStyles } from "@material-ui/core/styles";
import DataContex from "../context/DataContext";
import dateHelper from "@lib/data";

import insulationData from "@data/products/list";
import { typesData, connectorsData, wiresData } from "@data/features";

const useStyles = makeStyles({
  title: {
    margin: "35px",
    textAlign: "center",
    border: "1px solid #90B84A",
    fontWeight: "500",
    fontSize: "20px",
    padding: "20px 0 40px 0",
  },
});

export default function Table() {
  const classes = useStyles();
  const { data, level, getImagePath } = useContext(DataContex);

  const columns = [
    { field: "id", headerName: "ID", width: 120 },
    {
      field: "name",
      headerName: "Parametr",
      width: 290,
      editable: false,
    },
    {
      field: "choise",
      headerName: "Wybór",
      width: 150,
      editable: false,
    },
    {
      field: "photo",
      headerName: "Grafika",
      width: 250,
      editable: false,
      renderCell: (params) => {
        return (
          <div>
            <img
              style={{ width: "30%", marginTop: "30px", padding: "5px" }}
              src={getImagePath(params.row.photo)}
            />
          </div>
        );
      },
    },
  ];

  //1. insulationData
  //2. clampingData  //3. wireData
  //4. wireData
  const insulationObject = dateHelper.findItem(
    insulationData,
    data.insulationStripping
  );
  const clampingObject = dateHelper.findItem(typesData, data.clamping);

  const connectorsObject = dateHelper.findItem(connectorsData, data.connectors);

  const wiresObject = dateHelper.findItem(wiresData, data.wire);

  const rows = [
    {
      id: 1,
      name: "Narzędzie do ściągania izolacji",
      choise: data.insulationStripping,
      photo: insulationObject ? insulationObject.photo : "",
    },
    {
      id: 2,
      name: "Narzędzie do zaciskania złączy",
      choise: data.clamping,
      photo: clampingObject ? clampingObject.photo : "",
    },
    {
      id: 3,
      name: "Złącze",
      choise: data.connectors,
      photo: connectorsObject ? connectorsObject.photo : "",
    },
    {
      id: 4,
      name: "Przewód",
      choise: data.wire,
      photo: wiresObject ? wiresObject.photo : "",
    },
  ];

  return (
    <>
      <div className={classes.title}>
        Zależności między parametrami
        <div style={{ height: 265, margin: "20px auto" }}>
          <DataGrid
            localeText={plPL.props.MuiDataGrid.localeText}
            rows={rows}
            columns={columns}
            disableSelectionOnClick
          />
        </div>
      </div>
    </>
  );
}
