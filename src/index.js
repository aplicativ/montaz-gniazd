import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import reportWebVitals from "./reportWebVitals";

ReactDOM.render(<App getImagePath={(path) => path} />, document.getElementById("root"));

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();


// import React from "react";
// import ReactDOM from "react-dom";
// import "./index.css";
// import App from "./App";
// import reportWebVitals from "./reportWebVitals";
// import { SnackbarProvider } from "notistack";
// import { MuiThemeProvider, createTheme } from "@material-ui/core/styles";

// const theme = createTheme({
//   palette: {
//     primary: {
//       main: "#334957",
//     },
//     secondary: {
//       main: "#334957",
//     },
//   },
//   typography: {
//     fontFamily: ["OpenSans", "sans-serif"].join(","),
//     h2: {
//       fontSize: "24px",
//       fontWeight: "bold",
//       marginBottom: "10px",
//       textAlign: "center",
//       fontFamily: ["Oswald", "sans-serif"].join(","),
//     },
//   },
// });

// ReactDOM.render(
//   <MuiThemeProvider theme={theme}>
//     <SnackbarProvider
//       anchorOrigin={{
//         vertical: "bottom",
//         horizontal: "center",
//       }}
//     >
//       <App />
//     </SnackbarProvider>
//   </MuiThemeProvider>,
//   document.getElementById("root")
// );

// // If you want to start measuring performance in your app, pass a function
// // to log results (for example: reportWebVitals(console.log))
// // or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
// reportWebVitals();
