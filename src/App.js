import React, { useState } from "react";
import clsx from "clsx";
import CssBaseline from "@material-ui/core/CssBaseline";
import BottomNav from "./components/BottomNavigation";
import DataContex from "./context/DataContext";

import Header from "@components/Header";
import MainNavigation from "@components/MainNavigation";
import Content from "@components/Content";

import initialState from "./data/initialState";

import useStyles from "./styles";
import { SnackbarProvider } from "notistack";
import { MuiThemeProvider, createTheme } from "@material-ui/core/styles";

const theme = createTheme({
  palette: {
    primary: {
      main: "#334957",
    },
    secondary: {
      main: "#334957",
    },
  },
  typography: {
    fontFamily: ["OpenSans", "sans-serif"].join(","),
    h2: {
      fontSize: "24px",
      fontWeight: "bold",
      marginBottom: "10px",
      textAlign: "center",
      fontFamily: ["Oswald", "sans-serif"].join(","),
    },
  },
});

const App = ({ getImagePath }) => {
  const classes = useStyles();
  const [state, setState] = useState(initialState);
  const { level } = state;
  const { [level]: data } = state;
  const [isOpenNavigation, setIsOpenNavigation] = useState(false);

  return (
    <DataContex.Provider value={{ state, data, setState, level, getImagePath }}>
      <MuiThemeProvider theme={theme}>
        <SnackbarProvider
          anchorOrigin={{
            vertical: "bottom",
            horizontal: "center",
          }}
        >
          <div className={classes.newStackingContext}>
            <div className={classes.root}>
              <CssBaseline />

              <Header isOpenNavigation={isOpenNavigation} setIsOpenNavigation={setIsOpenNavigation} />
              <MainNavigation isOpenNavigation={isOpenNavigation} setIsOpenNavigation={setIsOpenNavigation} />

              <main
                className={clsx(classes.content, {
                  [classes.contentShift]: isOpenNavigation,
                })}
              >
                <div className={classes.drawerHeader} />
                <div className={classes.task}>
                  <Content />

                </div>
              </main>
            </div>
            <footer className={classes.footer}>
              <BottomNav />
            </footer>
          </div>
        </SnackbarProvider>
      </MuiThemeProvider>,
    </DataContex.Provider>
  );
}

export default App;
