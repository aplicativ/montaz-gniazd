import Swal from "sweetalert2";
import "./message.css";

const Message = {
  success: (title, text) => {
    Swal.fire({
      title,
      text:
        "Bardzo dobrze wykonałeś ćwiczenie. Widać, że dobrze opanowałeś materiał dotyczący doboru narzędzi i materiałów do montażu gniazd abonenckich RTV.",
      icon: "success",
      confirmButtonText: "Ok",
      backdrop: "rgba(65,172,38,0.3)",
    });
  },
  error: (title, text) => {
    Swal.fire({
      title,
      text:
        "Niestety, część odpowiedzi nie jest poprawna. Przeanalizuj ponownie materiały dotyczące doboru narzędzi i materiałów do montażu gniazd abonenckich RTV. Znajdziesz je w wizualizacjach w 2D/3D. Następnym razem na pewno dobrze wykonasz ćwiczenia. Powodzenia!",
      icon: `error`,
      confirmButtonText: "Ok",
    });
  },
  warning: (title, text) => {
    Swal.fire({
      title,
      text:
        "Niestety, tym razem nie wszystko poszło dobrze. Nie wybrałeś wszystkich poprawnych odpowiedzi. Przeanalizuj ponownie materiały zawarte w multimediach e-zasobu. Następnie spróbuj jeszcze raz wskazać parametry, z którymi miałeś problem. Powodzenia!",
      icon: "warning",
      confirmButtonText: "Spróbuj ponownie",
      confirmButtonColor: "red",
    });
  },
};

export default Message;
